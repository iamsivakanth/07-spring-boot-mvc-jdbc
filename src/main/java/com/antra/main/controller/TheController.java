package com.antra.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.antra.main.service.MyService;

@Controller
public class TheController {

	@Autowired
	MyService ser;

	@GetMapping(value = "/login")
	public String getLogin() {
		return "login";
	}

	@PostMapping(value = "/verify")
	public String verifyUser(@RequestParam String uname, @RequestParam String pword, Model model) {
		model.addAttribute("user", uname);

		boolean b = ser.verifyUser(uname, pword);
		if (b)
			return "success";
		else
			return "failed";
	}
}