package com.antra.main.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.main.dao.Dao;

@Service
public class ServiceImpl implements MyService {

	@Autowired
	Dao dao;

	@Override
	public boolean verifyUser(String uname, String pword) {
		Map map = dao.getUser(uname);
		if (map != null) {
			if ((Integer) map.get("activeuser") == 1) {
				if (map.get("pword").equals(pword)) {
					return true;
				} else
					return false;
			} else
				return false;
		} else
			return false;
	}
}