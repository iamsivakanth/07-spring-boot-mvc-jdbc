package com.antra.main.dao;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoImpl implements Dao {

	@Autowired
	JdbcTemplate template;

	@Override
	public Map getUser(String uname) {
		try {
		return template.queryForMap("select * from logindetails where uname = ?", uname);
		}
		catch (Exception e) {
			return null;
		}
	}
}